import { parse_file } from "./parse_file.ts";
import { execute } from "./execute.ts";

// cli tool, args are in Deno.args
const args = Deno.args;
// if we have the --help flag, print the help message
if (args.includes("--help")) {
    console.log("Usage: gbt [job]");
    Deno.exit(0);
}
const job = args[0];
const structure = await parse_file("GBuild");
if (job == undefined) {
    // run the default task
    execute(structure.tasks[structure.default]);
} else {
    // run the specified task
    execute(structure.tasks[job]);
}