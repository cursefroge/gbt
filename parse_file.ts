import { readLines } from "https://deno.land/std/io/mod.ts";

function getIndentationLevel(line: string): number {
    let level = 0;
    while (line[level] === ' ') {
        level++;
    }
    return level;
}

export async function parse_file(filePath: string): Promise<{default: string, tasks: Record<string, string[]>}> {
    const file = await Deno.open(filePath);
    const tasks: Record<string, string[]> = {};
    let currentTask = "";
    let currentIndentationLevel = 0;
    let defaultTask = "";

    for await (const line of readLines(file)) {
        const trimmedLine = line.trim();
        const indentationLevel = getIndentationLevel(line);

        if (trimmedLine.startsWith('#default:')) {
            defaultTask = trimmedLine.slice(10).trim();
        } else if (indentationLevel <= currentIndentationLevel && trimmedLine.endsWith(':')) {
            currentTask = trimmedLine.slice(0, -1);
            tasks[currentTask] = [];
            currentIndentationLevel = indentationLevel;
        } else if (currentTask && indentationLevel > currentIndentationLevel) {
            tasks[currentTask].push(trimmedLine);
        }
    }

    Deno.close(file.rid);
    return {default: defaultTask, tasks: tasks};
}