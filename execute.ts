export function execute(tasks: string[]) {
    // sequentially execute the tasks
    for (const task of tasks) {
        const process = Deno.run({
            cmd: task.split(' '), // split the task into command and arguments
        });
        process.status().then(() => process.close());
    }
}